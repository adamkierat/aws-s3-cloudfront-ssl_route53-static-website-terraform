variable "www_domain_name"{
    default = "www.adamkierat.pl"
}
variable "www_domain_name_without_www"{
    default = "adamkierat.pl"
}