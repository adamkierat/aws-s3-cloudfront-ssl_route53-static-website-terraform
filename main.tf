terraform {
  backend "s3" {
    region               = "eu-west-1"
    bucket               = "www.adamkierat.pl-terraform-state"
    encrypt              = true
    key                  = "tfstate"
    workspace_key_prefix = "prod"
  }
}
provider "aws" {
  region = "eu-west-1"
}
provider "aws" {
  region = "us-east-1"
  alias  = "us-east-1"
}
