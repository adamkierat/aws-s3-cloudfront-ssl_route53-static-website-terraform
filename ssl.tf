resource "aws_acm_certificate" "certificate" {
  provider          = aws.us-east-1
  domain_name       = var.www_domain_name_without_www
  validation_method = "DNS"

  subject_alternative_names = ["${var.www_domain_name}", "*.${var.www_domain_name_without_www}"]

  tags = {
    Environment = "prod"
  }
}

resource "aws_acm_certificate_validation" "certificate_validation" {
  provider                = aws.us-east-1
  certificate_arn         = aws_acm_certificate.certificate.arn
  validation_record_fqdns = [for record in aws_route53_record.certificate_validation_record : record.fqdn]
}
